# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/Sean/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keep class *.R
-keep public class tw.ponta.sdk.PONMemberCard

-keepclassmembers public class tw.ponta.sdk.PONMemberCard{
public static tw.ponta.sdk.PONMemberCard getInstance();
public void setContext(android.content.Context);
public void format();
public void show(java.lang.String,java.lang.String,android.view.ViewGroup);
}

-keepclassmembers public class tw.ponta.sdk.BaseView{
public void configure(android.app.Activity);
}

-keepclasseswithmembers class **.R$* {public static <fields>;}

