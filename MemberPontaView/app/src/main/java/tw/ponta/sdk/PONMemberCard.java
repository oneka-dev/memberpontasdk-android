package tw.ponta.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONObject;

import tw.ponta.util.VerticalTextView;

public class PONMemberCard extends BaseView {

    private Context m_Context;

    private static PONMemberCard m_PontaCardView = new PONMemberCard();

    private ImageLoader m_ImageLoader = ImageLoader.getInstance();

    private ImageLoaderConfiguration configuration = null;

    private ImageView m_ivBarcode = null;
    private ImageView m_ivCard = null;
    private ImageView m_ivLogo = null;
    private ImageView m_ivLMLogo = null;
    private ImageView m_ivBanner = null;
    private ImageView m_ivTicket = null;
    private ProgressBar m_pbView = null;
    private TextView m_tvNumber = null;
    private VerticalTextView m_tvCardNumber = null;
    private TextView m_tvError = null;
    private TextView m_tvPoint = null;
    private View pontaView = null;
    private View pontaErrView = null;
    private View pontaTicketView = null;
    private ViewGroup m_View = null;
    private RelativeLayout m_rlContent = null;

    private EditText m_etManuslly = null;

    private String m_strCardImageURL = null;
    private String m_strLogoImageURL = null;
    private String m_strADImageURL = null;
    private String m_strbarCodeURL = null;
    private String m_strADURL = null;
    private String m_strCardNumber = null;
    private String m_strPoints = null;
    private String m_strAPPID = null;
    private String m_strCustID = null;
    private int m_iVersion = 0;

    private Handler handler = null;
    private Runnable runnable = null;

    private String[] STR_FUNC_DIALOG = {WS_GET_CARDSINFO, WS_GET_STATUSEXAMINATION};

    private AlertDialog m_Dialog = null;

//    private final String strBanner = "http://event.ponta.com.tw/banner/app/sdk/default.png";
//    private final String strLogo = "https://dl.dropboxusercontent.com/s/zkga7srg3hjuzd0/CardLogo.png";
//    private final String strCard = "http://event.ponta.com.tw/card/008/a.png";
//    private final String strBarCode = "https://uat-www.ponta.com.tw/genbcServlet?msg=8888000000571901&height=10&hrp=none&fmt=jpg";
//    private final String strBanner = "https://dl.dropboxusercontent.com/s/b6qtbnje9qq4498/ad_info.png";

    public static PONMemberCard getInstance() {
        return m_PontaCardView;
    }

    public PONMemberCard() {
        super();
    }

    public void setContext(Context context) {
        super.setBaseViewContext(context);
        this.m_Context = context;
    }

    public void show(String strAPPID, String strCardNumber, ViewGroup view) {

        if (m_View != null) {
            this.m_View.removeView(pontaView);
        } else if (m_Context == null) {
            throw new NullPointerException("this context is null , Please setting \"setContext\"");
        }

        this.m_View = view;
        this.m_strAPPID = strAPPID;
        this.m_strCardNumber = strCardNumber;
        ((Activity) m_Context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        findView();
        this.m_View.addView(pontaView, new ViewGroup.LayoutParams(-1, -1));

        if (!isNetworkOK(m_Context)) {

            noWifi();

            return;
        }

        initLoader();

        SharedPreferences pre = getDefaultPreferences(m_Context);
        String strPrefAppID = pre.getString(m_strPrefAPPID, "");
        String strPrefCardNumber = pre.getString(m_strPrefCardNumber, "");

        if (!TextUtils.isEmpty(strPrefAppID) && !TextUtils.isEmpty(strPrefCardNumber)) {
            if (!strPrefAppID.equals(this.m_strAPPID) || !strPrefCardNumber.equals(this.m_strCardNumber)) {
                m_View.removeView(pontaView);
                pontaView = null;
                findView();
                this.m_View.addView(pontaView, new ViewGroup.LayoutParams(-1, -1));
                getStatusExamination(this.m_strCardNumber, this.m_strAPPID);
            }else{
                getStatusExamination(this.m_strCardNumber, this.m_strAPPID);
            }
        } else {
            getStatusExamination(this.m_strCardNumber, this.m_strAPPID);
        }

    }

    public void format() {
        super.format();
        this.pontaView = null;
        this.pontaErrView = null;
    }

    private void initLoader() {
        if (configuration == null) {
            configuration = ImageLoaderConfiguration.createDefault(m_Context);
            m_ImageLoader.init(configuration);
        }
    }

    private void findView() {
        if (pontaView == null) {
            pontaView = LayoutInflater.from(m_Context).inflate(R.layout.pontacard_view_jp_ticket, null);
            m_rlContent = (RelativeLayout) (pontaView.findViewById(R.id.rlContent));
            m_ivBarcode = (ImageView) (pontaView.findViewById(R.id.ivBarcard));
            m_ivCard = (ImageView) (pontaView.findViewById(R.id.ivCard));
            m_ivLMLogo = (ImageView) (pontaView.findViewById(R.id.ivLMLogo));
//            m_ivTicket = (ImageView) (pontaView.findViewById(R.id.ivTicket));
            m_ivLogo = (ImageView) (pontaView.findViewById(R.id.ivLogo));
            m_ivBanner = (ImageView) (pontaView.findViewById(R.id.ivBanner));
            m_pbView = (ProgressBar) (pontaView.findViewById(R.id.progressBar));
            m_tvNumber = (TextView) (pontaView.findViewById(R.id.tvNumber));
            m_tvPoint = (TextView) (pontaView.findViewById(R.id.tvPoint));
            m_tvCardNumber = (VerticalTextView) (pontaView.findViewById(R.id.tvCardNumber));

            onClicklisten();
            setProgressBar(m_pbView);
        }else{
            setProgressBar(null);
            m_rlContent.removeAllViews();
        }
    }

    protected boolean onDataRecovery(RecoveryData data) {
        if (super.onDataRecovery(data) == false) {
            String strErrMsg = m_Context.getResources().getString(R.string.err_str_other);
            setErrorView(strErrMsg);
            return false;
        }

        // JPCard API use
        for (int i = 0; i < STR_FUNC_DIALOG.length; i++) {
            if (STR_FUNC_DIALOG[i].compareToIgnoreCase(data.m_strCmd) == 0) {
                try {
                    String strErrID = data.m_jsonData.getString(JSON_TAG_ERROR_ID);

                    if (strErrID.compareToIgnoreCase(ERROR_CODE_SUCCESS) != 0) {

                        String strErrMsg = setErrorMsg(strErrID, m_Context.getResources().getString(R.string.err_str_system));

                        setErrorView(strErrMsg);
                        return false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    String strErrMsg = m_Context.getResources().getString(R.string.err_str_system);
                    setErrorView(strErrMsg);
                    return false;
                }
            }
        }

        if (data.m_strCmd.compareToIgnoreCase(WS_GET_STATUSEXAMINATION) == 0) {

            JSONObject jsonData = data.m_jsonData.optJSONObject("Content");

            if (jsonData != null) {
                m_strbarCodeURL = jsonData.optString("BarcodeURL");
                String strToken = jsonData.optString("Token");
                getDefaultPreferences(m_Context).edit()
                        .putString(m_strPrefToken, strToken)
                        .commit();
                getCardsInfo();
            }

        } else if (data.m_strCmd.compareToIgnoreCase(WS_GET_CARDSINFO) == 0) {

            JSONObject jsonData = data.m_jsonData.optJSONObject("Content");

            if (jsonData != null) {
                m_strPoints = jsonData.optString("Point");
                m_strCardImageURL = jsonData.optString("CardImageURL");
                m_strLogoImageURL = jsonData.optString("LogoImageURL");
                m_strCustID = jsonData.optString("custID");

                getDefaultPreferences(m_Context).edit()
                        .putString(m_strPrefAPPID, this.m_strAPPID)
                        .putString(m_strPrefCardNumber, this.m_strCardNumber)
                        .apply();

                JSONObject jsonADInfos = jsonData.optJSONObject("ADInfos");

                if (jsonADInfos != null) {
                    m_strADURL = jsonADInfos.optString("ADURL");
                    m_strADImageURL = jsonADInfos.optString("ADImageURL");
                }
                try {
                    m_strPoints = String.format("%,d", Integer.valueOf(m_strPoints));
                } catch (Exception e) {

                }
                setCustTextSize(m_tvNumber, m_strPoints);
                m_tvNumber.setText(m_strPoints);
                m_tvPoint.setText(m_Context.getResources().getString(R.string.eimi_point));
                setCustTextSize(m_tvCardNumber, addDashUtil(this.m_strCardNumber), 1);
                m_tvCardNumber.setText(addDashUtil(this.m_strCardNumber));
                loadImage(m_strbarCodeURL, m_strCardImageURL, m_strLogoImageURL, m_strADImageURL);
                m_ivLogo.setImageBitmap(BitmapFactory.decodeResource(m_Context.getResources(), R.drawable.logo));
                if(m_ivTicket != null)
                    m_ivTicket.setImageBitmap(BitmapFactory.decodeResource(m_Context.getResources(), R.drawable.btn_card_pointticket));
            }

        } else if (data.m_strCmd.compareToIgnoreCase(WS_GET_EXCHBONUSTICKET) == 0) {
            String strErrID = data.m_jsonData.optString(JSON_TAG_ERROR_ID);
            String strErrMsg = data.m_jsonData.optString(JSON_TAG_ERROR_MSG);

            if (strErrID.compareToIgnoreCase(ERROR_CODE_SUCCESS) != 0) {

                strErrMsg = setErrorMsg(strErrID, strErrMsg);

                m_Dialog = newAlertDialog(strErrMsg);
                m_Dialog.setCanceledOnTouchOutside(false);
                m_Dialog.setCancelable(true);
                m_Dialog.show();
                return false;
            } else {
                m_Dialog = newAlertDialog(strErrMsg, strErrID);
                m_Dialog.setCanceledOnTouchOutside(false);
                m_Dialog.setCancelable(true);
                m_Dialog.show();
                getCardsInfo();
            }
        }
        return true;
    }

    private void loadImage(String strUrlBarCode, String strUrlCard, String strUrlLogo, final String strUrlBanner) {
        m_ImageLoader.loadImage(strUrlBarCode, getSimpleOptions(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap

                if (loadedImage == null) {
                    loadedImage = loadBitmap(m_Context, m_strPrefBarcode);
                    if (loadedImage == null) {
                        String strErrMsg = m_Context.getResources().getString(R.string.err_str_system);
                        setErrorView(strErrMsg);
                        return;
                    }
                }
                Log.d("PontaSDK", loadedImage.getHeight() + " = hedith " + loadedImage.getWidth() + " = width");
                if (loadedImage.getWidth() > loadedImage.getHeight()) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    Bitmap bmBackNew = Bitmap.createBitmap(loadedImage, 0, 0, loadedImage.getWidth(),
                            loadedImage.getHeight(), matrix, true);
                    m_ivBarcode.setImageBitmap(bmBackNew);
                } else {
                    m_ivBarcode.setImageBitmap(loadedImage);
                }
                saveBitmap(m_Context, loadedImage, m_strPrefBarcode);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                super.onLoadingFailed(imageUri, view, failReason);
                String strErrMsg = m_Context.getResources().getString(R.string.err_str_other);
                setErrorView(strErrMsg);
            }
        });

        m_ImageLoader.loadImage(strUrlCard, getSimpleOptions(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap
                if (loadedImage == null) {
                    loadedImage = loadBitmap(m_Context, m_strPrefCardView);
                    if (loadedImage == null) {
                        return;
                    }
                }

                if (loadedImage.getWidth() < loadedImage.getHeight()) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    Bitmap bmBackNew = Bitmap.createBitmap(loadedImage, 0, 0, loadedImage.getWidth(),
                            loadedImage.getHeight(), matrix, true);
                    m_ivCard.setImageBitmap(bmBackNew);
                } else {
                    m_ivCard.setImageBitmap(loadedImage);
                }
                saveBitmap(m_Context, loadedImage, m_strPrefCardView);
            }
        });

        m_ImageLoader.loadImage(strUrlLogo, getSimpleOptions(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap
                if (loadedImage == null) {
                    loadedImage = loadBitmap(m_Context, m_strPrefLogo);
                    if (loadedImage == null) {
                        m_ivLMLogo.setImageBitmap(BitmapFactory.decodeResource(m_Context.getResources(), R.drawable.lm_logo));
                        return;
                    }
                }

                if (loadedImage.getWidth() > loadedImage.getHeight()) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    Bitmap bmBackNew = Bitmap.createBitmap(loadedImage, 0, 0, loadedImage.getWidth(),
                            loadedImage.getHeight(), matrix, true);
                    m_ivLMLogo.setImageBitmap(bmBackNew);
                } else {
                    m_ivLMLogo.setImageBitmap(loadedImage);
                }
                System.out.println(loadedImage+ " laodimage");
                saveBitmap(m_Context, loadedImage, m_strPrefLogo);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                super.onLoadingFailed(imageUri, view, failReason);

                m_ivLMLogo.setImageBitmap(BitmapFactory.decodeResource(m_Context.getResources(), R.drawable.lm_logo));

            }
        });

        m_ImageLoader.loadImage(strUrlBanner, getSimpleOptions(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap
                m_ivBanner.setImageBitmap(loadedImage);

                if (loadedImage == null) {
                    return;
                }

                m_ivBanner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!TextUtils.isEmpty(m_strADURL)) {
                            Uri uri = Uri.parse(m_strADURL);
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            m_Context.startActivity(intent);
                        }

                    }
                });
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                super.onLoadingFailed(imageUri, view, failReason);


            }
        });
    }

    private DisplayImageOptions getSimpleOptions() {

        DisplayImageOptions options = new DisplayImageOptions.Builder()

                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .build();

        return options;

    }

    private void setErrorView(String strError) {
        this.m_View.removeAllViews();
        pontaErrView = LayoutInflater.from(m_Context).inflate(R.layout.pontacard_err_view, null);
        m_tvError = (TextView) (pontaErrView.findViewById(R.id.tvErrorView));
        m_tvError.setText(strError);
        this.m_View.addView(pontaErrView);
    }

    private void noWifi() {
        Bitmap bmBarcode = loadBitmap(m_Context, m_strPrefBarcode);
        if (bmBarcode == null) {
            String strErrMsg = m_Context.getResources().getString(R.string.err_str_other);
            setErrorView(strErrMsg);
            return;
        }

        if (bmBarcode.getWidth() > bmBarcode.getHeight()) {
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap bmBackNew = Bitmap.createBitmap(bmBarcode, 0, 0, bmBarcode.getWidth(), bmBarcode.getHeight(), matrix, true);
            m_ivBarcode.setImageBitmap(bmBackNew);
        }

        Bitmap bmCardView = loadBitmap(m_Context, m_strPrefCardView);
        Bitmap bmLogo = loadBitmap(m_Context, m_strPrefLogo);
        m_ivCard.setImageBitmap(bmCardView);
        m_ivLMLogo.setImageBitmap(BitmapFactory.decodeResource(m_Context.getResources(), R.drawable.lm_logo));
        m_ivLogo.setImageBitmap(BitmapFactory.decodeResource(m_Context.getResources(), R.drawable.logo));
        if(m_ivTicket != null)
            m_ivTicket.setImageBitmap(BitmapFactory.decodeResource(m_Context.getResources(), R.drawable.btn_card_pointticket));
        m_ivBanner.setImageBitmap(null);

        m_tvPoint.setText("");
        m_tvNumber.setText("");

        String strCardNumber = getDefaultPreferences(m_Context).getString(m_strPrefCardNumber, "");

        if (strCardNumber.equals(this.m_strCardNumber)) {
            setCustTextSize(m_tvCardNumber, addDashUtil(this.m_strCardNumber), 1);
            m_tvCardNumber.setText(addDashUtil(this.m_strCardNumber));
        } else {
            String strErrMsg = m_Context.getResources().getString(R.string.err_str_other);
            setErrorView(strErrMsg);
        }
    }

    /**
     * System init PopupWindow
     * ticket import
     **/
    private void initPopupWindow() {
        pontaTicketView = LayoutInflater.from(m_Context).inflate(R.layout.view_coupon_import, null);

        final PopupWindow pop = new PopupWindow(pontaTicketView,
                pontaView.getWidth(),
                pontaView.getHeight(),
                true);

        pop.setBackgroundDrawable(new ColorDrawable(0xFFFFFF));
        pop.setClippingEnabled(true);

        int[] location = new int[2];
        pontaView.getLocationOnScreen(location);
        int x = location[0];
        int y = location[1];
        pop.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        pop.showAtLocation(pontaView, Gravity.NO_GRAVITY, x, y);
        pop.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int x = (int) event.getX();
                final int y = (int) event.getY();
                if(x > 0 && y > 0 && x < pontaView.getWidth() && y < pontaView.getHeight()){
                    return false;
                }else{
                    return true;
                }
            }
        });

        m_etManuslly = (EditText) pontaTicketView.findViewById(R.id.etManuslly);
        Button m_btReturn = (Button) pontaTicketView.findViewById(R.id.btnReturn);
        ProgressBar m_pbTicketView = (ProgressBar) pontaTicketView.findViewById(R.id.progressBar);
        pontaTicketView.findViewById(R.id.btnSend).setOnClickListener(onClickTicketView);

        setProgressBar(m_pbTicketView);

        m_btReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pop.dismiss();
            }
        });

        pop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                setProgressBar(m_pbView);
            }
        });
    }

    /**
     * System init PopupWindow
     * ticket import
     **/
    //ViewGroup add View
    private void initADDTicketView() {
        pontaTicketView = LayoutInflater.from(m_Context).inflate(R.layout.view_coupon_import, null);
        m_rlContent.addView(pontaTicketView);
        m_etManuslly = (EditText) pontaTicketView.findViewById(R.id.etManuslly);
        pontaTicketView.findViewById(R.id.btnReturn).setOnClickListener(onClickTicketView);
        pontaTicketView.findViewById(R.id.btnSend).setOnClickListener(onClickTicketView);
    }

    private View.OnClickListener onClickTicketView = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int i = v.getId();
            if (i == R.id.btnReturn) {
                m_rlContent.removeView(pontaTicketView);
            } else if (i == R.id.btnSend) {
                String strManuslly = m_etManuslly.getText().toString();
                if (!strManuslly.isEmpty()) {
                    exchBonusTicket(m_strCustID , strManuslly);
                } else {
                    m_Dialog = newAlertDialog(m_Context.getResources().getString(R.string.err_str_ticket_empty));
                    m_Dialog.setCancelable(true);
                    m_Dialog.setCanceledOnTouchOutside(false);
                    m_Dialog.show();
                }

            }else if (i == R.id.ivLMLogo){
                if(handler == null){
                    handler = new Handler();
                    runnable = new Runnable( ) {
                        public void run ( ) {
                            m_iVersion = 0;
                        }
                    };
                }

                if (m_iVersion < 4) {
                    handler.removeCallbacks(runnable);
                    handler.postDelayed(runnable,2000);
                    m_iVersion ++;
                } else {
                    m_iVersion = 0;

                    Toast.makeText(m_Context,m_Context.getResources().getString(R.string.sdk_version),Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    private String setErrorMsg(String strErrId, String strErrMsg) {

        if (strErrId.compareToIgnoreCase(ERROR_CODE_CONNECTION_TIMEOUT) == 0) {

            strErrMsg = m_Context.getResources().getString(R.string.err_str_other);

        } else if (strErrId.compareToIgnoreCase(ERROR_CODE_JSON_FORMAT_ERROR) == 0) {

            strErrMsg = m_Context.getResources().getString(R.string.err_str_system);

        } else if (strErrId.compareToIgnoreCase(ERROR_CODE_JSON_PARSE_ERROR) == 0) {

            strErrMsg = m_Context.getResources().getString(R.string.err_str_param);

        } else if (strErrId.compareToIgnoreCase(ERROR_CODE_MEMBER) == 0) {

            strErrMsg = m_Context.getResources().getString(R.string.err_str_member);
        }

        return strErrMsg;
    }

    private AlertDialog newAlertDialog(String strMsg, String strErrId) {
        return newAlertDialog(strMsg, removeTicketViewClick);
    }

    private DialogInterface.OnClickListener removeTicketViewClick = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            m_rlContent.removeAllViews();
        }
    };


    private void onClicklisten(){
        if(m_ivTicket !=null){
            m_ivTicket.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isNetworkOK(m_Context)){
                        initPopupWindow();
                    }
                }
            });
        }
        m_ivLMLogo.setOnClickListener(onClickTicketView);

    }

}
