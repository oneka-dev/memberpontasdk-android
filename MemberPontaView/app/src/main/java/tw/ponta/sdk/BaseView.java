package tw.ponta.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class BaseView extends HttpRequesJson {
	protected final String JSON_TAG_ERROR_ID = "RetCode";
	protected final String JSON_TAG_ERROR_MSG = "RetMSG";

	private final int MSG_DO_DATA_RECOVERY = 1;

	protected final String ERROR_CODE_SUCCESS = "0000";
	protected final String ERROR_CODE_MEMBER = "0001";
	protected final String ERROR_CODE_CONNECTION_TIMEOUT = "8999";
	protected final String ERROR_CODE_JSON_PARSE_ERROR = "0002";
	protected final String ERROR_CODE_JSON_FORMAT_ERROR = "9999";

	private int m_iBlockingReqCnt = 0;

	protected final String WS_GET_CARDSINFO = "/api/cardsInfo";
	protected final String WS_GET_STATUSEXAMINATION = "/api/statusExamination";
	protected final String WS_GET_EXCHBONUSTICKET = "/seaCaretta/api/exchBonusTicket.do";

	protected final String m_strPrefLogo = "eimi_pre_logo";
	protected final String m_strPrefBarcode = "eimi_pre_barcode";
	protected final String m_strPrefCardView = "eimi_pre_cardview";
	protected final String m_strPrefCardNumber = "eimi_pre_card_number";
	protected final String m_strPrefAPPID = "eimi_pre_appid";
	protected final String m_strPrefToken = "eimi_pre_token";
	protected final String m_strPrefCustID = "eimi_pre_custid";
	private Boolean DEBUG_MODE;
	private String LOG_TAG = "PONTA_SDK";

	private String m_url = "https://uat-www.ponta.com.tw";

	private Context m_Context;
	private ProgressBar m_ProgressBar = null;
	private Handler m_handler;

	protected void setBaseViewContext(Context context) {
		super.setHttpRequesJsonContext(context);
		this.m_Context = context;
	}

	public BaseView() {
		this.m_handler = new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what == MSG_DO_DATA_RECOVERY) {
					onDataRecovery((RecoveryData) msg.obj);
				}
			}
		};
	}

	public void postURLNonBlocking(final String strCmd, final String strURL, final JSONObject jsonQuery, boolean isFormData) {
		postURLRequest(strCmd, strURL, jsonQuery, false, isFormData);
	}

	public void postURL(final String strCmd, final String strURL, final JSONObject jsonQuery, boolean isFormData) {
		postURLRequest(strCmd, strURL, jsonQuery, true, isFormData);
	}

	public void getStatusExamination(String strCardNumber, String strAPPID) {
		postURL(WS_GET_STATUSEXAMINATION, getBaseURLString(WS_GET_STATUSEXAMINATION), packJsonStatusExamination(strCardNumber, strAPPID), false);
	}

	public void getCardsInfo() {
		postURLNonBlocking(WS_GET_CARDSINFO, getBaseURLString(WS_GET_CARDSINFO), packJsonCardsInfo(m_strPrefToken), false);
	}

	public void exchBonusTicket(String strCustId, String strManuslly) {
		postURL(WS_GET_EXCHBONUSTICKET, getBaseURLString(WS_GET_EXCHBONUSTICKET), packJsonExchBonusTicket(strCustId, "N99XYQZ28ATMAVZTJRMV"), true);
	}

	public String getBaseURLString(String strWS) {
		m_url = m_Context.getResources().getString(R.string.PONTA_URL);
		return String.format("%s%s", m_url, strWS);
	}


	private void postURLRequest(final String strCmd, final String strURL, final JSONObject jsonQuery, final boolean bBlocking, final boolean isFormData) {

		if (bBlocking == true) {
			m_iBlockingReqCnt++;
			if (this.m_ProgressBar != null)
				this.m_ProgressBar.setVisibility(View.VISIBLE);
		}

		if (isFormData) {
			new Thread() {
				public void run() {
					JSONObject jsonObj = postAndGetJsonData(strURL, jsonQuery, isFormData);
					doDataRecovery(strCmd, strURL, jsonObj, bBlocking);
				}
			}.start();
		} else {
			new Thread() {
				public void run() {
					JSONObject jsonObj = postAndGetJsonData(strURL, jsonQuery);
					doDataRecovery(strCmd, strURL, jsonObj, bBlocking);
				}
			}.start();
		}
	}

	private class SSLSocketFactoryEx extends SSLSocketFactory {
		SSLContext sslContext = SSLContext.getInstance("TLS");

		public SSLSocketFactoryEx(KeyStore truststore)
				throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
			super(truststore);

			TrustManager tm = new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
						throws java.security.cert.CertificateException {
				}

				@Override
				public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
						throws java.security.cert.CertificateException {
				}
			};

			sslContext.init(null, new TrustManager[]{tm}, null);
		}

		@Override
		public Socket createSocket(Socket socket, String host, int port, boolean autoClose)
				throws IOException, UnknownHostException {
			return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
		}

		@Override
		public Socket createSocket() throws IOException {
			return sslContext.getSocketFactory().createSocket();
		}
	}

	private HttpClient newHttpsClient() {
		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);

			SSLSocketFactory sf = new SSLSocketFactoryEx(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

			return new DefaultHttpClient(ccm, params);
		} catch (Exception e) {
			return new DefaultHttpClient();
		}
	}

	private HttpClient newHttpClient(String strURL) {
		if (strURL.startsWith("https") == true) {
			return newHttpsClient();
		}

		return new DefaultHttpClient();
	}

	public JSONObject postAndGetJsonData(final String strURL, final JSONObject jsonQuery) {
		return postAndGetJsonData(strURL, jsonQuery, false);
	}

	//true setCarrtta
	public JSONObject postAndGetJsonData(final String strURL, final JSONObject jsonQuery, boolean isFormData) {
		final HttpClient httpClient = newHttpClient(strURL);

		// disable request retry
		((DefaultHttpClient) httpClient).setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(0, false));

		// request parameters
		HttpParams params = httpClient.getParams();
		HttpConnectionParams.setConnectionTimeout(params, 10000);
		HttpConnectionParams.setSoTimeout(params, 10000);
		// set parameter
		HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

		HttpPost httpPost;
		String strErrCode = null;
		String strResp;
		JSONObject jsonObj = null;
		try {
			httpPost = new HttpPost(strURL);
			StringEntity se;

			if (jsonQuery != null) {
				se = new StringEntity(jsonQuery.toString(), HTTP.UTF_8);
			} else {
				se = new StringEntity(new JSONObject().toString(), HTTP.UTF_8);
			}

			if (isFormData) {
				List<NameValuePair> parameters = new ArrayList<NameValuePair>();
				Iterator<String> it = jsonQuery.keys();

				while (it.hasNext()) {
					String key = it.next();
					try {
						parameters.add(new BasicNameValuePair(key, jsonQuery.getString(key)));
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				httpPost.setEntity(new UrlEncodedFormEntity(parameters, HTTP.UTF_8));
				httpPost.setHeader("Accept", "text/html");
				httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
				httpPost.setHeader("User-Agent", "OneCard1.0.0");

			} else {
				httpPost.setEntity(se);
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json; charset=utf-8");
				httpPost.setHeader("User-Agent", "OneCard1.0.0");
			}

			// Response handler
			ResponseHandler<String> rh = new ResponseHandler<String>() {
				// invoked when client receives response
				public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
					// get response entity
					HttpEntity entity = response.getEntity();

					StringBuffer out = new StringBuffer();
					byte[] b = EntityUtils.toByteArray(entity);
					out.append(new String(b, 0, b.length, "utf-8"));
					return out.toString();
				}
			};
			strResp = httpClient.execute(httpPost, rh);
			// remove non-ASCII char
			strResp = strResp.replaceAll("\\P{Print}", "");

			if (strResp == null || strResp.length() == 0) {
				jsonObj = new JSONObject();
				jsonObj.put(JSON_TAG_ERROR_ID, ERROR_CODE_JSON_FORMAT_ERROR);
			} else if (strResp.charAt(0) == '[' || strResp.charAt(0) == '{') {
				jsonObj = new JSONObject(strResp);
			} else {
				jsonObj = new JSONObject();
				jsonObj.put(JSON_TAG_ERROR_ID, ERROR_CODE_JSON_FORMAT_ERROR);
			}
		} catch (JSONException ejson) {
			ejson.printStackTrace();

			strErrCode = ERROR_CODE_JSON_PARSE_ERROR;
			jsonObj = new JSONObject();
		} catch (EOFException eof) {
			eof.printStackTrace();
			strErrCode = ERROR_CODE_JSON_PARSE_ERROR;
			jsonObj = new JSONObject();
		} catch (Exception e) {
			e.printStackTrace();
			strErrCode = ERROR_CODE_CONNECTION_TIMEOUT;
			jsonObj = new JSONObject();
		} finally {
			try {
				if (jsonObj != null && strErrCode != null) {
					jsonObj.put(JSON_TAG_ERROR_ID, strErrCode);
				}
			} catch (Exception e) {
				jsonObj = null;
			}
		}

		return jsonObj;
	}

	// Run on Main thread
	protected boolean onDataRecovery(RecoveryData data) {

		if (data.m_jsonData == null) {
			return false;
		}

		if (data.m_bBlocking == true) {
			m_iBlockingReqCnt--;
		}
		System.out.println(m_iBlockingReqCnt + " m " + m_ProgressBar);
		if (m_iBlockingReqCnt == 0) {
			if (this.m_ProgressBar != null)
				this.m_ProgressBar.setVisibility(View.GONE);
		}

		return true;
	}

	private void doDataRecovery(final String strCmd, final String strURL, final JSONObject jsonData, final boolean bBlocking) {

		m_handler.obtainMessage(MSG_DO_DATA_RECOVERY, new RecoveryData(strCmd, strURL, jsonData, bBlocking)).sendToTarget();

	}

	public static class TextSizeAdjuster {
		public static final int MAX_INFINITE = -1;

		private TextSizeAdjuster() {
		}

		public static float adjustForWidth(String var0, int var1, float var2) {
			return a(var0, var1, var2, 0);
		}

		public static float adjustForHeight(String var0, int var1, float var2) {
			return a(var0, var1, var2, 1);
		}

		private static float a(String var0, int var1, float var2, int var3) {
			Paint var4 = new Paint();
			float var5 = 10.0F;
			Rect var7 = new Rect();

			while (true) {
				var4.setTextSize(var5);
				int var6;
				if (var3 == 0) {
					var6 = (int) var4.measureText(var0);
				} else {
					var4.getTextBounds(var0, 0, var0.length(), var7);
					var6 = var7.bottom - var7.top;
				}

				if (var6 >= var1) {
					var5 -= 0.5F;
					break;
				}

				var5 += 0.5F;
				if (var2 != -1.0F && var5 > var2) {
					var5 = var2;
					break;
				}
			}

			return var5;
		}
	}

	public void setProgressBar(ProgressBar view) {
		if (view == null) {
			if (m_ProgressBar != null) {
				m_ProgressBar.setVisibility(View.GONE);
			}
		}
		this.m_ProgressBar = view;
	}

	public class RecoveryData {
		public String m_strCmd = null;
		public String m_strURL = null;
		public JSONObject m_jsonData = null;
		public boolean m_bBlocking = false;

		public RecoveryData(String strCmd, String strURL, JSONObject jsonData, boolean bBlocking) {
			m_strCmd = strCmd;
			m_strURL = strURL;
			m_jsonData = jsonData;
			m_bBlocking = bBlocking;
		}
	}

	public void saveBitmap(Context context, Bitmap bitmap, String strName) {
		Bitmap mBitamp = bitmap; //保存したいBitmap
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
		String bitmapStr = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);

		SharedPreferences pref = getDefaultPreferences(context);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(strName, bitmapStr);
		editor.apply();
	}

	protected Bitmap loadBitmap(Context context, String strName) {

		SharedPreferences pref = getDefaultPreferences(context);
		String s = pref.getString(strName, "");
		Bitmap bitmap = null;
		if (!s.equals("")) {
//			BitmapFactory.Options options = new BitmapFactory.Options();
			byte[] b = Base64.decode(s, Base64.DEFAULT);
			bitmap = BitmapFactory.decodeByteArray(b, 0, b.length).copy(Bitmap.Config.ARGB_8888, true);
		}
		return bitmap;
	}

	protected static SharedPreferences getDefaultPreferences(Context var0) {
		return var0.getSharedPreferences("eimi_ponta_sdk", 0);
	}

	protected void setCustTextSize(TextView textView, String strContent) {
		float var7 = (float) ((textView.getBaseline() << 2) / 5);
		int var3 = textView.getWidth() - textView.getPaddingLeft() - textView.getPaddingRight();
		float var8 = BaseView.TextSizeAdjuster.adjustForWidth(strContent, var3, var7);
		textView.setTextSize(0, var8);
	}

	//type everything number
	protected void setCustTextSize(TextView textView, String strContent, int type) {
		float fTextBaseline = (float) ((textView.getBaseline() << 2) / 5);
		int iTextWidth = textView.getHeight() - textView.getPaddingLeft() - textView.getPaddingRight();
		float fTextSize = BaseView.TextSizeAdjuster.adjustForWidth(strContent, iTextWidth, fTextBaseline);
		textView.setTextSize(0, fTextSize);
	}

	protected String addDashUtil(String strNumber) {
		StringBuilder str = new StringBuilder(strNumber);
		int idx = str.length() - 4;

		while (idx > 0) {
			str.insert(idx, "-");
			idx = idx - 4;
		}

		return str.toString();
	}

	protected void format() {
		SharedPreferences.Editor var1;
		(var1 = getDefaultPreferences(m_Context).edit()).clear();
		var1.commit();
	}

	//configure light
	public void configure(Activity var1) {
		Window var2 = var1.getWindow();
		var2.addFlags(128);
		WindowManager.LayoutParams var4;
		(var4 = var2.getAttributes()).screenBrightness = 1.0F;
		var2.setAttributes(var4);
	}

	//default
	protected AlertDialog newAlertDialog(String strMsg) {
		return newAlertDialog(strMsg, defaultClick);
	}

	//default
	protected DialogInterface.OnClickListener defaultClick = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.dismiss();
		}
	};

	//default
	protected AlertDialog newAlertDialog(String strMsg, DialogInterface.OnClickListener yeslistener) {
		return new AlertDialog.Builder(m_Context)
//                .setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle(m_Context.getResources().getString(R.string.dialog_title))
				.setMessage(strMsg)
				.setPositiveButton(m_Context.getResources().getString(R.string.dialog_ok), yeslistener)
				.create();
	}

	protected Boolean isNetworkOK(Context context) {
		ConnectivityManager cManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = cManager.getActiveNetworkInfo();
		if (info != null && info.isAvailable()) {
			return true;
		} else {
			return false;
		}
	}

	protected void log(String strLog) {
		DEBUG_MODE = m_Context.getResources().getBoolean(R.bool.debug_mode);
		LOG_TAG = m_Context.getResources().getString(R.string.log_tag);

		if (DEBUG_MODE == true) {
			Log.i(LOG_TAG, strLog);
		}
	}
}
