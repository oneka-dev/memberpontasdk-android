package tw.ponta.sdk;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

public class HttpRequesJson {
	private Context m_Context;
	private Boolean DEBUG_MODE = true;
	private String LOG_TAG = "PONTA_SDK";

	public HttpRequesJson() {
		super();
	}

	protected void setHttpRequesJsonContext(Context context) {
		this.m_Context = context;
	}

	protected JSONObject packJsonStatusExamination(String strCardNumber, String strAPPID) {
		JSONObject obj = new JSONObject();

		try {
			obj.put("cardNumber", strCardNumber);
			obj.put("APPID", strAPPID);
		} catch (Exception e) {
			e.printStackTrace();
			obj = null;
		}

		return obj;
	}

	protected JSONObject packJsonCardsInfo(String strPrefName) {
		JSONObject obj = new JSONObject();

		String strToken = PONMemberCard.getDefaultPreferences(m_Context).getString(strPrefName, "");

		try {
			obj.put("token", strToken);

		} catch (Exception e) {
			e.printStackTrace();
			obj = null;
		}

		return obj;
	}

	protected JSONObject packJsonExchBonusTicket(String strCustId, String strTicketId) {
		JSONObject obj = new JSONObject();

		try {
			obj.put("custId", strCustId);
			obj.put("ticketId", strTicketId);

		} catch (Exception e) {
			e.printStackTrace();
			obj = null;
		}

		return obj;
	}

	protected void log(String strLog) {

		DEBUG_MODE = m_Context.getResources().getBoolean(R.bool.debug_mode);
		LOG_TAG = m_Context.getResources().getString(R.string.log_tag);

		if (DEBUG_MODE == true) {
			Log.i(LOG_TAG, strLog);
		}
	}
}
