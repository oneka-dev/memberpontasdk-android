package tw.ponta.util;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextView extends TextView {
    private boolean a = false;

    public CustomTextView(Context var2) {
        super(var2);
    }

    public CustomTextView(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    @Override
    public void setGravity(int var1) {
        super.setGravity(var1);
    }

    @Override
    protected void onDraw(Canvas var1) {

        this.a = (getGravity() & 112) == 80;
        if(this.a) {
            var1.translate(0.0F, (float)(this.getHeight() - this.getBaseline() - 4));
        }

        super.onDraw(var1);
    }
}

