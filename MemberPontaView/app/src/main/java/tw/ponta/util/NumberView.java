package tw.ponta.util;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;

import tw.ponta.sdk.BaseView;

public class NumberView extends CustomTextView {

	public NumberView(Context var2) {
		super(var2);
		this.setTextColor(Color.rgb(105, 105, 105));
		this.setSingleLine(true);
		this.setMaxLines(1);
		this.setPadding(0, 0, 0, 0);
	}

	public NumberView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	protected void onMeasure(int var1, int var2) {
		super.onMeasure(var1, var2);
		this.setMeasuredDimension(MeasureSpec.getSize(var1), MeasureSpec.getSize(var2));
	}

	protected void onLayout(boolean var1, int var2, int var3, int var4, int var5) {
		super.onLayout(var1, var2, var3, var4, var5);

		if (var1) {
			String var6;
			var6 = this.getText().toString();

			float var7 = (float) ((this.getBaseline() << 2) / 5);
			var3 = this.getWidth() - this.getPaddingLeft() - this.getPaddingRight();
			float var8 = BaseView.TextSizeAdjuster.adjustForWidth(var6, var3, var7);
			this.setTextSize(0, var8);

		}
	}
}
